CREATE COMPUTE MODULE CommonComponents_MQAttach_Logging_Construct_AuditLogEvent

CREATE FUNCTION Main() RETURNS BOOLEAN

BEGIN

DECLARE rOutRfh REFERENCE TO OutputLocalEnvironment;

DECLARE rOutTran REFERENCE TO OutputLocalEnvironment;

DECLARE rOutTrnSet REFERENCE TO OutputLocalEnvironment;

DECLARE rOutMetaDta REFERENCE TO OutputLocalEnvironment;

DECLARE rOutCall REFERENCE TO OutputLocalEnvironment;

DECLARE rOutClRest REFERENCE TO OutputLocalEnvironment;

DECLARE rEnv REFERENCE TO Environment.Variables;

DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;

DECLARE cStatusMsg CHARACTER 'success';

DECLARE cTs CHARACTER;

DECLARE cTts CHARACTER CURRENT_GMTTIMESTAMP;

DECLARE cStartTimeStamp CHARACTER;

DECLARE cSTts CHARACTER;

/* Change the status message to failed if status is failure*/

IF rMqaLogEvt.StatusCode <> 0 THEN

SET cStatusMsg = 'failed';

END IF;

/* Check the tranactionId*/

IF rMqaLogEvt.TransactionId IS NULL THEN

SET rMqaLogEvt.TransactionId = UUIDASCHAR;

END IF;

SET cStartTimeStamp = CAST(rMqaLogEvt.FlowStartTime AS CHARACTER);

SET cSTts = SUBSTRING (cStartTimeStamp FROM 15 FOR 10)||'T'|| SUBSTRING (cStartTimeStamp FROM 26 FOR 12 );

SET cTs = SUBSTRING (cTts FROM 15 FOR 10)||'T'|| SUBSTRING (cTts FROM 26 FOR 12 );

-- Construct MQRFH2 Header with topic string

--Check availability of Info log in Environment.Variables.InfoLog.MQA.LogEvent

IF EXISTS(rMqaLogEvt.InfoEventMessages[]) THEN

CREATE LASTCHILD OF OutputRoot.XMLNSC AS rOutTran NAME 'transactionPayload';

SET rOutTran.(XMLNSC.Attribute)ID = rMqaLogEvt.TransactionId;

SET rOutTran.EventMessages = rMqaLogEvt.InfoEventMessages;

PROPAGATE TO TERMINAL 'out1' ;

END IF;

--delete the previously constructed output message as it was already sent for logging

DELETE FIELD OutputRoot.XMLNSC;

/*Construct MQAttach Message*/

CREATE LASTCHILD OF OutputRoot.XMLNSC AS rOutTran NAME 'transaction';

SET rOutTran.(XMLNSC.Attribute)ID = rMqaLogEvt.TransactionId;

SET rOutTran.(XMLNSC.Attribute)version = '5.00';

SET rOutTran.(XMLNSC.Attribute)agentRole = 'callAgent';

SET rOutTran.(XMLNSC.NamespaceDecl)xmlns:xsi = xsi;

SET rOutTran.(XMLNSC.NamespaceDecl)xsi:noNamespaceSchemaLocation = 'TransferLog.xsd';

SET rOutTran.action.(XMLNSC.Attribute)time = cTs || 'Z';

SET rOutTran.action = 'completed';

-- Agent Info

SET rOutTran.agent.(XMLNSC.Attribute)agent = SQL.BrokerName || '.' || UCASE(SQL.ExecutionGroupLabel) ;

SET rOutTran.agent.(XMLNSC.Attribute)QMgr = SQL.QueueManagerName ;

SET rOutTran.agent.systemInfo.(XMLNSC.Attribute)name = SQL.BrokerName ;

SET rOutTran.agent.systemInfo.(XMLNSC.Attribute)version = SQL.BrokerVersion ;

SET rOutTran.agent.systemInfo.(XMLNSC.Attribute)architecture = SQL.Family ;

-- Orginator Info

SET rOutTran.originator.hostName = SQL.BrokerName;

SET rOutTran.originator.userID = LCASE(SQL.BrokerUserId) ;

-- Status info

SET rOutTran.status.(XMLNSC.Attribute)resultCode = rMqaLogEvt.StatusCode;

SET rOutTran.status.supplement = rMqaLogEvt.ResponseMsg;

-- TransferSet Info

CREATE LASTCHILD OF rOutTran AS rOutTrnSet NAME 'transferSet';

SET rOutTrnSet.(XMLNSC.Attribute)bytesSent = rMqaLogEvt.MessageByteCount;

SET rOutTrnSet.(XMLNSC.Attribute)total = '1';

SET rOutTrnSet.(XMLNSC.Attribute)startTime = cSTts || 'Z';

-- Create MetaData

FOR itrMetaDta AS rMqaLogEvt.MetaDataSet.MetaData[] DO

CREATE LASTCHILD OF rOutTrnSet.metaDataSet AS rOutMetaDta NAME 'metaData';

SET rOutMetaDta = itrMetaDta.Value;

SET rOutMetaDta.(XMLNSC.Attribute)key = itrMetaDta.Key;

END FOR;

-- Call Info

CREATE LASTCHILD OF rOutTrnSet AS rOutCall NAME 'call';

SET rOutCall.command.(XMLNSC.Attribute)name = SQL.MessageFlowLabel;

SET rOutCall.command.(XMLNSC.Attribute)successRC = rMqaLogEvt.StatusCode;

SET rOutCall.command.(XMLNSC.Attribute)retryCount= '0';

SET rOutCall.command.(XMLNSC.Attribute)retryWait= '0';

SET rOutCall.command.(XMLNSC.Attribute)type = 'WMB_Flow';

SET rOutCall.command.argument = 'none';

-- Call Result Info

CREATE LASTCHILD OF rOutCall AS rOutClRest NAME 'callResult';

SET rOutClRest.(XMLNSC.Attribute)outcome = cStatusMsg;

SET rOutClRest.(XMLNSC.Attribute)retries = '0';

SET rOutClRest.result.(XMLNSC.Attribute)outcome = cStatusMsg;

SET rOutClRest.result.(XMLNSC.Attribute)returnCode = rMqaLogEvt.StatusCode;

SET rOutClRest.result.(XMLNSC.Attribute)time = cTs || 'Z';

-- Create Exception or Result Message

FOR itrErrDta AS rMqaLogEvt.ErrorDetails.ErrorMessage[] DO

CREATE LASTCHILD OF rOutClRest.result.stdout NAME 'line' VALUE itrErrDta;

END FOR;

IF EXISTS(rMqaLogEvt.ErrorDetails.ExceptionListDetails[]) THEN

CREATE LASTCHILD OF rOutClRest.result.stderr NAME 'line' VALUE rMqaLogEvt.ErrorDetails.ExceptionListDetails;

END IF;

SET rOutTran.job.name = SQL.MessageFlowLabel;

--Capturing Statistics

SET rOutTran.statistics.actualStartTime = cSTts || 'Z';

SET rOutTran.statistics.retryCount = '0';

SET rOutTran.statistics.numFileFailures = '0';

SET rOutTran.statistics.numFileWarnings = '0';

--Send the AuditMetaData for logging

RETURN TRUE;

END;

END MODULE;