CREATE COMPUTE MODULE ConstructPingResponse

CREATE FUNCTION Main() RETURNS BOOLEAN

/* --------------------------------------------------------------------------------

* Func/Proc Name : ConstructPingResponse

* Description : This node is used to construct Ping Response message.

* Input : Ping request, External Variables - APPVERSION,BUILDVERSION,APPENV

* Output : Ping response.

* Modification History :

* DATE (MM/DD/CCYY) Author Comments

Cognizant Initial Creation

--------------------------------------------------------------------------------*/

BEGIN

--declare variables

SET OutputRoot.HTTPInputHeader = NULL;

DECLARE outHdrRsp Reference To OutputRoot;

DECLARE pingRsp REFERENCE TO OutputRoot;

DECLARE pingReq REFERENCE TO InputRoot.XMLNSC.v11:Ping;

DECLARE rEnv REFERENCE TO Environment.Variables;

DECLARE returnCode INT 0;

DECLARE returnMessage CHARACTER 'Success';

DECLARE detailMessage CHARACTER 'Transaction is success';

--create output strcuture

CREATE LASTCHILD OF OutputRoot AS outHdrRsp DOMAIN 'SOAP' NAME 'SOAP';

--construct service header response

CALL BuildServiceHeaderResponse (outHdrRsp, rEnv.msgHeaderInfo, returnCode,returnMessage, detailMessage);

--create output response

CREATE LASTCHILD OF OutputRoot.SOAP AS pingRsp NAME 'Body';

CREATE LASTCHILD OF pingRsp AS pingRsp NAMESPACE v11 NAME 'PingResponse';

SET pingRsp.v12:PingResponseMessage.v12:Environment = APPENV;

--If DetailedCheck true then add IIB Node Name, IS Name to Ping response

IF (pingReq.v12:PingRequestMessage.v12:DetailedCheck = TRUE) THEN

SET Environment.Variables.PingRequest.DetailedCheck = TRUE;

SET pingRsp.v12:PingResponseMessage.v12:IntegrationNode = SQL.BrokerName;

SET pingRsp.v12:PingResponseMessage.v12:IntegrationServerName = UCASE(SQL.ExecutionGroupLabel);

--Send to out1 terminal for EndPointConnectivity check

PROPAGATE TO TERMINAL 'out1' DELETE NONE;

END IF;

SET pingRsp.v12:PingResponseMessage.v12:BuildVersion = BUILDVERSION;

SET pingRsp.v12:PingResponseMessage.v12:AppVersion = APPVERSION;

--Set EndPointCheck result

SET pingRsp.v12:PingResponseMessage.v12:EndPointConnectivity=rEnv.EndPointConnectivity;

--Append Request message to response

SET pingRsp.v12:PingResponseMessage.v12:ResponseMessage = 'You sent: '||COALESCE(pingReq.v12:PingRequestMessage.v12:RequestMessage,'');

--MQAttach logging - Info Log

CALL AddInfoLogEvent(NodeLabel,'Response sent to consumer',rEnv);

--Audit the Transaction end time

DECLARE totalTransactionTime Interval (CURRENT_TIMESTAMP - rEnv.Transaction.StartTime) second;

--MQAttach logging - update Info log with Transaction duration

CALL AddInfoLogEvent(NodeLabel,'Total transaction execution time: '||SUBSTRING(CAST(totalTransactionTime AS CHAR) AFTER ' '),rEnv);

RETURN TRUE;

END;

END MODULE;