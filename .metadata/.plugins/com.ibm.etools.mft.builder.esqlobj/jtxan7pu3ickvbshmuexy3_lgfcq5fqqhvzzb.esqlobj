CREATE COMPUTE MODULE CAG_Member_Lookup_v3_ConstructMemberLookupReq

CREATE FUNCTION Main() RETURNS BOOLEAN

BEGIN

CREATE FIELD Environment.Variables;

DECLARE rEnv REFERENCE TO Environment.Variables;

DECLARE inRequestHeaderRef REFERENCE TO InputRoot.HTTPInputHeader;

DECLARE flag BOOLEAN;

SET rEnv.Transaction.StartTime = getCurrentTimestamp();

-- fetch the Environment Indicator to route

SET rEnv.env = CAST(inRequestHeaderRef.Environmentindicator AS CHARACTER);

-- setting the URL's

IF (rEnv.env = 'ppmo') THEN

SET rEnv.MEMBERURL = MEMBERLOOKUP_PPMO_EDL;

SET rEnv.PCPURL = PCPLOOKUP_PPMO_EDL;

SET rEnv.COBURL = COBLOOKUP_PPMO_EDL;

SET rEnv.AORURL = AORLOOKUP_PPMO_EDL;

SET rEnv.INVOICEURL = INVOICELOOKUP_PPMO_EDL;

ELSE

SET rEnv.MEMBERURL = MEMBERLOOKUP_EDL;

SET rEnv.PCPURL = PCPLOOKUP_EDL;

SET rEnv.COBURL = COBLOOKUP_EDL;

SET rEnv.AORURL = AORLOOKUP_EDL;

SET rEnv.INVOICEURL = INVOICELOOKUP_EDL;

END IF;

--Store HTTP Header parameters into environment

CALL StoreHTTPRequestHeaders(rEnv, inRequestHeaderRef);

DECLARE ipLocalEnvRef REFERENCE TO InputLocalEnvironment.HTTP.Input.QueryString;

DECLARE memberId,limit,mbi,firstName,lastName,gender,state,city,zipCode,address1,dob,homePhone,isEmployee,workPhone,cellPhone,benefitPlanId CHARACTER;

SET memberId= UCASE(ipLocalEnvRef.memberId);

SET mbi = UPPER(ipLocalEnvRef.mbi);

SET firstName = ipLocalEnvRef.firstName;

SET lastName = ipLocalEnvRef.lastName;

SET gender = ipLocalEnvRef.gender;

SET state = ipLocalEnvRef.state;

SET city = ipLocalEnvRef.city;

SET zipCode = ipLocalEnvRef.zipCode;

SET address1 = ipLocalEnvRef.address1;

SET dob = ipLocalEnvRef.dob;

SET homePhone = ipLocalEnvRef.homePhone;

SET isEmployee = ipLocalEnvRef.isEmployee;

SET workPhone = ipLocalEnvRef.workPhone;

SET cellPhone = ipLocalEnvRef.cellPhone;

SET benefitPlanId = ipLocalEnvRef.benefitPlanId;

SET limit = ipLocalEnvRef.limit;

--logging input fields in Environment

SET rEnv.memberId = ipLocalEnvRef.memberId;

SET rEnv.mbi = ipLocalEnvRef.mbi;

SET rEnv.firstName = ipLocalEnvRef.firstName;

SET rEnv.lastName = ipLocalEnvRef.lastName;

SET rEnv.gender = ipLocalEnvRef.gender;

SET rEnv.state = ipLocalEnvRef.state;

SET rEnv.city = ipLocalEnvRef.city;

SET rEnv.zipCode = ipLocalEnvRef.zipCode;

SET rEnv.address1 = ipLocalEnvRef.address1;

SET rEnv.dob = ipLocalEnvRef.dob;

SET rEnv.homePhone = ipLocalEnvRef.homePhone;

SET rEnv.isEmployee = ipLocalEnvRef.isEmployee;

SET rEnv.workPhone = ipLocalEnvRef.workPhone;

SET rEnv.cellPhone = ipLocalEnvRef.cellPhone;

SET rEnv.benefitPlanId = ipLocalEnvRef.benefitPlanId;

SET rEnv.mode = TRIM(COALESCE(ipLocalEnvRef.mode,''));

SET rEnv.limit = COALESCE(ipLocalEnvRef.limit,'200');

--Set source system

DECLARE sourceSystem CHARACTER COALESCE(inRequestHeaderRef."Eh-Header-Sourcesystem",'Not Received');

DECLARE clientApplication CHARACTER COALESCE(inRequestHeaderRef."Eh-Header-ClientApplication",'Not Received');

CALL CreateAuditLogEvent(rEnv);

--MQAttach logging - Log the Audit MetaData

CALL CreateAuditMetaData(sourceSystem,clientApplication,memberId,firstName,lastName,gender,state,city,zipCode,address1,dob,homePhone,workPhone,isEmployee,cellPhone,limit,benefitPlanId,rEnv);

--MQAttach logging - Info Log

CALL AddInfoLogEvent(NodeLabel,'Request received from consumer',rEnv);

--MQAttach logging - Log the message with payload

DECLARE bLoginputHeader BLOB;

SET bLoginputHeader = ASBITSTREAM(inRequestHeaderRef CCSID rEnv.MQA.CodedCharSetId ENCODING rEnv.MQA.Encoding);

CALL AddPayloadLogEvent(bLoginputHeader,NodeLabel,'Request message from consumer.','text',rEnv);

-- constructing memberlookup request to EDL

DECLARE msgRef REFERENCE TO OutputRoot;

CREATE LASTCHILD OF OutputRoot AS msgRef DOMAIN 'JSON' NAME 'JSON';

CREATE FIELD OutputRoot.JSON.Data;

DECLARE outRef REFERENCE TO OutputRoot.JSON.Data;

-- validating the Input parameters

IF (TRIM(rEnv.memberId) IS NOT NULL AND TRIM(rEnv.memberId) <> '') OR

(TRIM(rEnv.mbi) IS NOT NULL AND TRIM(rEnv.mbi) <> '') OR

(TRIM(rEnv.firstName) IS NOT NULL AND TRIM(rEnv.firstName) <> '') OR

(TRIM(rEnv.lastName) IS NOT NULL AND TRIM(rEnv.lastName) <> '') OR

(TRIM(rEnv.gender) IS NOT NULL AND TRIM(rEnv.gender) <> '') OR

(TRIM(rEnv.state) IS NOT NULL AND TRIM(rEnv.state) <> '') OR

(TRIM(rEnv.city) IS NOT NULL AND TRIM(rEnv.city) <> '') OR

(TRIM(rEnv.zipCode) IS NOT NULL AND TRIM(rEnv.zipCode) <> '') OR

(TRIM(rEnv.address1) IS NOT NULL AND TRIM(rEnv.address1) <> '') OR

(TRIM(rEnv.dob) IS NOT NULL AND TRIM(rEnv.dob) <> '') OR

(TRIM(rEnv.homePhone) IS NOT NULL AND TRIM(rEnv.homePhone) <> '') OR

(TRIM(rEnv.workPhone) IS NOT NULL AND TRIM(rEnv.workPhone) <> '') OR

(TRIM(rEnv.isEmployee) IS NOT NULL AND TRIM(rEnv.isEmployee) <> '') OR

(TRIM(rEnv.mode) IS NOT NULL AND TRIM(rEnv.mode) <> '') OR

(TRIM(rEnv.benefitPlanId) IS NOT NULL AND TRIM(rEnv.benefitPlanId) <> '') OR

(TRIM(rEnv.cellPhone) IS NOT NULL AND TRIM(rEnv.cellPhone) <> '') THEN

SET flag = TRUE;

ELSE

SET flag = FALSE;

END IF;

-- If all search parameters are valid construct memberlookup request

IF (flag) THEN

CREATE FIELD outRef.query.bool.must IDENTITY (JSON.Array)must;

DECLARE k INTEGER 1;

-- mode value check

-- implementing altid+reccode search for mode1

IF rEnv.mode = '1' THEN

IF COALESCE(rEnv.memberId,'') <> '' AND LENGTH(rEnv.memberId)=11 THEN

DECLARE pdpAltId CHARACTER SUBSTRING(memberId FROM 1 FOR 9);

DECLARE pdpRecCode CHARACTER SUBSTRING(memberId FROM 10);

SET outRef.query.bool.must.Item[k].match.vsamMbrAltId = pdpAltId;

SET k=k+1;

SET outRef.query.bool.must.Item[k].match.recCode = pdpRecCode;

SET k=k+1;

-- cagEligrnk,pcprnk for filerting memberId's

SET outRef.query.bool.must.Item[k].match.cagEligrnk = '1';

SET k=k+1;

SET outRef.query.bool.must.Item[k].match.pcprnk = '1';

SET k=k+1;

DECLARE bLogBenefitRequest BLOB;

--MQAttach logging - Request payload

SET bLogBenefitRequest = ASBITSTREAM(msgRef.Data CCSID codedCharSetId ENCODING rEnv.MQA.Encoding) ;

CALL AddPayloadLogEvent(bLogBenefitRequest,NodeLabel,'Benefit Lookup Request sent to EDL via Mode-1 search.','text',rEnv);

--MQAttach logging - Info Log

CALL AddInfoLogEvent(NodeLabel,'Benefit Lookup Request sent to EDL via Mode-1 search',rEnv);

--Audit the Transaction start time for EDL Invoke

SET rEnv.Transaction.BenefitlookupStartTime = getCurrentTimestamp();

-- setting the URL

CALL AddInfoLogEvent(NodeLabel,'EDL MemberURL Invoked for Mode-1 is '||CAST(rEnv.MEMBERURL AS CHAR),rEnv);

SET OutputLocalEnvironment.Destination.HTTP.RequestURL = rEnv.MEMBERURL;

PROPAGATE TO TERMINAL 'out1' DELETE NONE;

RETURN FALSE;

ELSE

SET rEnv.invalidMemberId = 'TRUE';

SET rEnv.ErrorDesc = 'Invalid search criteria, pass only valid Alternate Member Id';

THROW USER EXCEPTION MESSAGE 4001 VALUES ('Invalid search criteria, pass only valid Alternate Member Id');

END IF;

-- implementing LIKE search for mode 0

ELSEIF TRIM(rEnv.mode) = '' OR TRIM(rEnv.mode) = '0' THEN

DECLARE J INTEGER 1;

-- EQUAL search by mbi

IF (TRIM(rEnv.mbi) IS NOT NULL AND TRIM(rEnv.mbi) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrMbi = ipLocalEnvRef.mbi;

SET k=k+1;

END IF;

-- LIKE search by firstName

IF (TRIM(rEnv.firstName) IS NOT NULL AND TRIM(rEnv.firstName) <> '') THEN

SET outRef.query.bool.must.Item[k].regexp."mbrTblFirstName.normalize" = ipLocalEnvRef.firstName||'.*';

SET k=k+1;

END IF;

-- LIKE search by lastName

IF (TRIM(rEnv.lastName) IS NOT NULL AND TRIM(rEnv.lastName) <> '') THEN

SET outRef.query.bool.must.Item[k].regexp."mbrTblLastName.normalize" = ipLocalEnvRef.lastName||'.*';

SET k=k+1;

END IF;

-- EQUAL search by dob

IF (TRIM(rEnv.dob) IS NOT NULL AND TRIM(rEnv.dob) <> '') THEN

-- converting the CAG date format 'yyyy-MM-dd' to EDL format 'MM/dd/yyyy'

DECLARE mbrDob DATE CAST(rEnv.dob AS DATE FORMAT 'yyyy-MM-dd');

DECLARE mDob CHARACTER CAST(mbrDob AS CHARACTER FORMAT 'MM/dd/yyyy');

SET outRef.query.bool.must.Item[k].match.mbrTblDobDate = mDob;

SET k=k+1;

END IF;

-- EQUAL search by city

IF (TRIM(rEnv.city) IS NOT NULL AND TRIM(rEnv.city) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblAddrCityName = ipLocalEnvRef.city;

SET k=k+1;

END IF;

-- EQUAL search by zipcode

IF (TRIM(rEnv.zipCode) IS NOT NULL AND TRIM(rEnv.zipCode) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblAddrZipCode = ipLocalEnvRef.zipCode;

SET k=k+1;

END IF;

-- EQUAL search by address1

IF (TRIM(rEnv.address1) IS NOT NULL AND TRIM(rEnv.address1) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblAddrStreetName1 = ipLocalEnvRef.address1;

SET k=k+1;

END IF;

-- EQUAL search by state

IF (TRIM(rEnv.state) IS NOT NULL AND TRIM(rEnv.state) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblAddrStateCode = ipLocalEnvRef.state;

SET k=k+1;

END IF;

-- EQUAL search by genderId

IF (TRIM(rEnv.gender) IS NOT NULL AND TRIM(rEnv.gender) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblGenderInd = ipLocalEnvRef.gender;

SET k=k+1;

END IF;

-- EQUAL search by homePhone

IF (TRIM(rEnv.homePhone) IS NOT NULL AND TRIM(rEnv.homePhone) <> '') THEN

SET outRef.query.bool.must.Item[k].match.homePhone = ipLocalEnvRef.homePhone;

SET k=k+1;

END IF;

-- EQUAL search by workPhone

IF (TRIM(rEnv.workPhone) IS NOT NULL AND TRIM(rEnv.workPhone) <> '') THEN

SET outRef.query.bool.must.Item[k].match.workPhoneNumber = ipLocalEnvRef.workPhone;

SET k=k+1;

END IF;

-- EQUAL search by cellPhone

IF (TRIM(rEnv.cellPhone) IS NOT NULL AND TRIM(rEnv.cellPhone) <> '') THEN

SET outRef.query.bool.must.Item[k].match.phone = ipLocalEnvRef.cellPhone;

SET k=k+1;

END IF;

-- EQUAL search by benefitPlanId

IF (TRIM(rEnv.benefitPlanId) IS NOT NULL AND TRIM(rEnv.benefitPlanId) <> '') THEN

SET outRef.query.bool.must.Item[k].match.mbrTblPbpNo = ipLocalEnvRef.benefitPlanId;

SET k=k+1;

END IF;

-- EQUAL search by isEmployee

IF (TRIM(rEnv.isEmployee) IS NOT NULL AND TRIM(rEnv.isEmployee) <> '') THEN

IF (TRIM(rEnv.isEmployee) = 'Y') THEN

SET outRef.query.bool.must.Item[k].match.isEmployee = 'Yes';

ELSEIF(TRIM(rEnv.isEmployee) = 'N') THEN

SET outRef.query.bool.must.Item[k].match.isEmployee = 'No';

ELSE

SET outRef.query.bool.must.Item[k].match.isEmployee = ipLocalEnvRef.isEmployee;

END IF;

SET k=k+1;

END IF;

--cagEligrnk,pcprnk for filerting memberId's

SET outRef.query.bool.must.Item[k].match.cagEligrnk = '1';

SET k=k+1;

SET outRef.query.bool.must.Item[k].match.pcprnk = '1';

SET k=k+1;

IF (TRIM(rEnv.memberId) IS NOT NULL AND TRIM(rEnv.memberId) <> '') THEN

CREATE FIELD outRef.query.bool.must.Item[k].bool.should IDENTITY (JSON.Array)should;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."memberId.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."vsamMbrAltId.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."mbrTblPpoAltId.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."hixId.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."cinNumber.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."mbrTblNmiId.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."mbrTblHicnNo.normalize" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

--fix for searching CAG PPO Member Id

SET outRef.query.bool.must.Item[k].bool.should.Item[J].regexp."cagPpoMemberId" = ipLocalEnvRef.memberId||'.*';

SET J=J+1;

SET k=k+1;

END IF;

SET outRef.size = 800;

ELSE

SET rEnv.invalidMemberId = 'TRUE';

SET rEnv.ErrorDesc = 'Invalid search criteria, unknown mode value';

THROW USER EXCEPTION MESSAGE 4001 VALUES ('Invalid search criteria, unknown mode value');

END IF;

-- else throw search criteria exception

ELSE

SET rEnv.invalidMemberId = 'TRUE';

SET rEnv.ErrorDesc = 'Invalid search criteria, pass valid arguments';

THROW USER EXCEPTION MESSAGE 4001 VALUES('Invalid search criteria, pass valid arguments');

END IF;

DECLARE bLogBenefitRequest BLOB;

--MQAttach logging - Request payload

SET bLogBenefitRequest = ASBITSTREAM(msgRef.Data CCSID codedCharSetId ENCODING rEnv.MQA.Encoding) ;

CALL AddPayloadLogEvent(bLogBenefitRequest,NodeLabel,'Benefit Lookup Request sent to EDL via Mode-0 search.','text',rEnv);

--MQAttach logging - Info Log

CALL AddInfoLogEvent(NodeLabel,'Benefit Lookup Request sent to EDL via Mode-0 search',rEnv);

--Audit the Transaction start time for EDL Invoke

SET rEnv.Transaction.BenefitlookupStartTime = getCurrentTimestamp();

-- set the URL

CALL AddInfoLogEvent(NodeLabel,'EDL MemberURL Invoked for Mode-0 is '||CAST(rEnv.MEMBERURL AS CHAR),rEnv);

SET OutputLocalEnvironment.Destination.HTTP.RequestURL = rEnv.MEMBERURL;

RETURN TRUE;

END;

/*This used to capture the audit metadata and payload from incoming requests in order to log in MQAttach.*/

CREATE PROCEDURE CreateAuditMetaData(IN sourceSystem CHARACTER,IN clientApplication CHARACTER,IN memberId CHARACTER,IN firstName CHARACTER,IN lastName CHARACTER,IN gender CHARACTER,IN state CHARACTER,IN city CHARACTER,IN zipCode CHARACTER,IN address1 CHARACTER,IN

dob CHARACTER,IN homePhone CHARACTER,IN isEmployee CHARACTER,IN workPhone CHARACTER,IN cellPhone CHARACTER,IN limit CHARACTER,IN benefitPlanId CHARACTER,INOUT rEnv REFERENCE)

BEGIN

--Call procedure to construct AuditMetdata and store in environment

CALL CreateMetaData('TransactionId',rEnv."Eh-Header-Transactionid",'TransactionId',rEnv);

CALL CreateMetaData('ParentTransactionId',rEnv."Eh-Header-ParentTransactionid",'ParentTransactionId',rEnv);

CALL CreateMetaData('TransactionTimestamp',rEnv."Eh-Header-TransactionTimestamp",'TransactionTimestamp',rEnv);

CALL CreateMetaData('Header-ClientApplication',clientApplication,'Header-ClientApplication',rEnv);

CALL CreateMetaData('Header-SourceSystem',sourceSystem,'Header-SourceSystem',rEnv);

CALL CreateMetaData('EndUserId',rEnv."Eh-Header-Enduserid",'EndUserId',rEnv);

CALL CreateMetaData('Header-Tenant-Region',rEnv."Eh-Header-Tenant-Region",'Header-Tenant-Region',rEnv);

CALL CreateMetaData('Header-Tenant-SubRegion',rEnv."Eh-Header-Tenant-Subregion",'Header-Tenant-SubRegion',rEnv);

CALL CreateMetaData('Header-Tenant-Partner',rEnv."Eh-Header-Tenant-Partner",'Header-Tenant-Partner',rEnv);

CALL CreateMetaData('Header-Tenant-Id',rEnv."Eh-Header-Tenant-ID",'Header-Tenant-Id',rEnv);

CALL CreateMetaData('MemeberId',memberId,'MemeberId',rEnv);

CALL CreateMetaData('firstName',firstName,'firstName',rEnv);

CALL CreateMetaData('lastName',lastName,'lastName',rEnv);

CALL CreateMetaData('gender',gender,'gender',rEnv);

CALL CreateMetaData('state',state,'state',rEnv);

CALL CreateMetaData('city',city,'city',rEnv);

CALL CreateMetaData('zipCode',zipCode,'zipCode',rEnv);

CALL CreateMetaData('address1',address1,'address1',rEnv);

CALL CreateMetaData('dob',dob,'dob',rEnv);

CALL CreateMetaData('homePhone',homePhone,'homePhone',rEnv);

CALL CreateMetaData('isEmployee',isEmployee,'isEmployee',rEnv);

CALL CreateMetaData('workPhone',workPhone,'workPhone',rEnv);

CALL CreateMetaData('cellPhone',cellPhone,'cellPhone',rEnv);

CALL CreateMetaData('benefitPlanId',benefitPlanId,'benefitPlanId',rEnv);

CALL CreateMetaData('limit',limit,'limit',rEnv);

END;

END MODULE;