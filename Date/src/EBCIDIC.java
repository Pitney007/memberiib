import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;




public class EBCIDIC {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		String frequency = "1";
		String strProcessingCompany = "011";
		String strPolicyNumber = "ADP112415A";
		boolean isEnrollment = true;
		boolean creditCardDeclinedFlag = true;
		//boolean strEFTFlag = true;
		String strEFTFlag = "Y";
		String strPaymentMode = "2";
		
		PackedDecimal  PackedConvertor = new PackedDecimal();
		System.out.println ("I am Starting");
		System.out.println("Frequency - "+frequency);
		System.out.println("strProcessingCompany - "+strProcessingCompany);
			
		String RecordLength = "    "; 
		String systemcode = "K";   
		String companyCode = "";
		if(strProcessingCompany.equalsIgnoreCase("011")){ // changing from AGL to 011 as we are getting 011 in the request.
			companyCode = "11";
		}else if(strProcessingCompany.equalsIgnoreCase("012")){ // changing from USL to 012 as we are getting 012 in the request.
			companyCode = "12";
		}
//		String strPolicyNumber = "1234512345";  // Have to pass the policy number herer dynamically.
		String strCheckDigit = "ZZZZ";
		
		//The transaction code should be converted to packed decimal before FTPing...
		String strTransactionCode = "97";
		

		
		// Concatenated 
		String strConCatField1 = systemcode+companyCode+strPolicyNumber+strCheckDigit;
		System.out.println("Check StringConcat field 1 " + strConCatField1);
		
		// Convert the elements based on the following rule...
		// Rule 1 - Convert all COMP-3 fields to packed decimalal - For example: Transaction code
		// Rule 2  - Convert all NON - comp-3 fields to EBCDIC....
		
		//Converter EBDCIDICconverter = new Converter();
		byte[] result2 = Converter.convertStringToEBCDIC(strConCatField1.getBytes());
		System.out.println("Checking converted concatfield1 " + result2);	
		byte[] packedresult2 = PackedConvertor.pack(strTransactionCode);
		
		
		byte[] c = new byte[result2.length + packedresult2.length];
		System.arraycopy(result2, 0, c, 0, result2.length);
		System.arraycopy(packedresult2, 0, c, result2.length, packedresult2.length);
		System.out.println("Intermediate Res array c-------->"+new String(c));
		
		String strCostCenter = "    ";
		String strTranType = "  ";

//		byte[] byteTranAndCostCenter = Converter.convertStringToEBCDIC((strTranType+strcontrolAG+strCostCenter).getBytes());
		byte[] byteTranAndCostCenter = Converter.convertStringToEBCDIC((strTranType+strCostCenter).getBytes());
		
			byte[] d = new byte[c.length + byteTranAndCostCenter.length];
		System.arraycopy(c, 0, d, 0, c.length);
		System.arraycopy(byteTranAndCostCenter, 0, d, c.length, byteTranAndCostCenter.length);
		System.out.println("Intermediate Res array d-------->"+new String(d));
		//strCycleDate  needs to be converted to packed value...
		
		DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		Date date = new Date();
		System.out.println("Date -"+dateFormat.format(date)); 
		
		byte[] byteCycleDate =  PackedConvertor.pack("1" + dateFormat.format(date).toString());
		
		byte[] e = new byte[d.length + byteCycleDate.length];
		System.arraycopy(d, 0, e, 0, d.length);
		System.arraycopy(byteCycleDate, 0, e, d.length, byteCycleDate.length);	
		
		System.out.println("Intermediate Res array e -------->"+new String(e));
		byte[] byteBatchNo =  PackedConvertor.pack("001");
		
		byte[] f = new byte[e.length + byteBatchNo.length];
		System.arraycopy(e, 0, f, 0, e.length);
		System.arraycopy(byteBatchNo, 0, f, e.length, byteBatchNo.length);	
		System.out.println("Intermediate Res array f -------->"+new String(f));
		
		String strcontrolAG = "4044    ";
		
		//Build Frequency string - This value needs to be dynamic from the BO.
		System.out.println("Frequency - "+frequency);
		System.out.println("Frequency length- "+frequency.length());
		
		String strFrequency = frequency.trim();   // Policy service returns the Agnis value as it is so no trnasformation is needed.
		
		// Change as per new Requirement // Luis/Mohon
		// TODO : If It is termination and Card is declined then only change it to 3 else keep the frequncvy as it is.
		//creditCardDeclinedFlag = true
		//isEnrollment = flase
		if (strFrequency.equalsIgnoreCase("1") && !isEnrollment && creditCardDeclinedFlag) {
		//if(strFrequency.equalsIgnoreCase("1")){
			strFrequency = "3";
		}
		
		
		//Changing the frequency in case of termination by user and going to direct pay and frequncy is monthly.
		if (strFrequency.equalsIgnoreCase("1") && !isEnrollment && strEFTFlag.equalsIgnoreCase("N")) {
			strFrequency = "3";
		}
		
		System.out.println("strFrequency -"+strFrequency);
			
		// Mapping Frequency from Chase in case of Enrollment
		if (isEnrollment) {
			if (strPaymentMode.equalsIgnoreCase("1")) {
				strFrequency = "2";
			} else if (strPaymentMode.equalsIgnoreCase("2")) {
				strFrequency = "6";
			} else if (strPaymentMode.equalsIgnoreCase("3")) {
				strFrequency = "3";
			} else if (strPaymentMode.equalsIgnoreCase("4")) {
				strFrequency = "1";
			}
		}
		
//		String strTransactionMethod = "61";  //  needs to change this as dynamic value;
	System.out.println("strEFTFlag - "+strEFTFlag);
	String strEnrollmentIndicator = "";

	if(isEnrollment == true){
		strEnrollmentIndicator = "Y";
		
	}
	else
	{
		strEnrollmentIndicator = "N";
	}

	System.out.println("After setting Enrollment Indicator");


	String strTransactionMethod = ""; 
	// TODO change this to isEnrollment = false and EFT Flag was Y
		//if(strEFTFlag.equalsIgnoreCase("Y")){
		 if(!isEnrollment && strEFTFlag.equalsIgnoreCase("Y")){
			strTransactionMethod = "61"; 
		}else{
			strTransactionMethod = "10"; 
		}


		// strTransactionPayMode  includes first three bytes of group num includes 3 spaces, 1 spaces for group POS and 9 fillers and 7 more fillers
		
		String strTransactionPayMode = "RCC   ";  // need to add a if loop to passs RCC when iut is RCC otherwise Spaces.
		// As per new Requirement confimed by Luis/Mohan
		System.out.println("Credit Card declined -"+creditCardDeclinedFlag);
		/*if(creditCardDeclinedFlag == true){
			strTransactionPayMode = "      ";
		}
		if(!strEnrollmentIndicator.equalsIgnoreCase("Y")){
			strTransactionPayMode = "      ";	
		}*/
		// Change this code to reflect Transaction Pay mode as default spaces in case of Termination and Credit card decline.
		if (! isEnrollment) {
			strTransactionPayMode = "      ";
		}
		if(creditCardDeclinedFlag == true){
			strTransactionPayMode = "      ";
		}
		

		String strTransactionPGroupPos7 = " ";
		String fillers = "                ";
			
		String strConcatenateTrans1 = strcontrolAG+strFrequency+strTransactionMethod+strTransactionPayMode+strTransactionPGroupPos7+fillers;
		System.out.println("strConcatenateTrans1 "+strConcatenateTrans1 );
		byte[]  byteFrequency = Converter.convertStringToEBCDIC(strConcatenateTrans1.getBytes());
		
		byte[] h = new byte[f.length + byteFrequency.length];
		System.arraycopy(f, 0, h, 0, f.length);
		System.arraycopy(byteFrequency, 0, h, f.length, byteFrequency.length);	
		//Transbill date as spaces. AGNIS should pick up the bill day from the master file....
		byte[] Transbill = PackedConvertor.pack("000");
		System.out.println("Intermediate Res array h -------->"+new String(h));
		
		
		
		byte[] k = new byte[h.length + Transbill.length];
		System.arraycopy(h, 0, k, 0, h.length);
		System.arraycopy(Transbill, 0, k, h.length, Transbill.length);	
		String StrTranExchangIND = " ";
		String strFiller1 = "  ";
		byte[]  bytestrFiller1 = Converter.convertStringToEBCDIC((StrTranExchangIND + strFiller1).getBytes());
		System.out.println("Intermediate Res array k -------->"+new String(k));	
		
		byte[] l = new byte[k.length + bytestrFiller1.length];
		System.arraycopy(k, 0, l, 0, k.length);
		System.arraycopy(bytestrFiller1, 0, l, k.length, bytestrFiller1.length);	
		
		// Transaction type - needs to be drived based on the billing method change
		// If EFT to RCC - pass Y
		// otherwise pass spaces..
		
		// This is incorrect 
		// Need to check what the EFT flag 
		// LHS is on the billing mode	
		
		String strTransaction250mode = " ";
		if(isEnrollment && strEFTFlag.equalsIgnoreCase("Y")){ // TODO : Change this condition to isEnrollement and  strEFTFlag = Y confirm with doug-
			strTransaction250mode = "Y";
		}
		String strTranAUTOCOLL = " ";
		String strFiller2 = "    ";
		String strconcateTransmode = strTransaction250mode+strTranAUTOCOLL+strFiller2;
		
		byte[]  	bytesstrTransaction250mode = Converter.convertStringToEBCDIC(strconcateTransmode.getBytes());
		
		byte[] m = new byte[l.length + bytesstrTransaction250mode.length];
		System.arraycopy(l, 0, m, 0, l.length);
		System.arraycopy(bytesstrTransaction250mode, 0, m, l.length, bytesstrTransaction250mode.length);
		System.out.println("Intermediate Res array m -------->"+new String(m));
		
		
		byte[] byteFinalPacks1 = PackedConvertor.pack("000");
		
		byte[] n = new byte[m.length + byteFinalPacks1.length];
		System.arraycopy(m, 0, n, 0, m.length);
		System.arraycopy(byteFinalPacks1, 0, n, m.length, byteFinalPacks1.length);
		System.out.println("Intermediate Res array n -------->"+new String(n));
		
		byte[] byteFinalPacks2 = PackedConvertor.pack("00000");
		
		byte[] o = new byte[n.length + byteFinalPacks2.length];
		System.arraycopy(n, 0, o, 0, n.length);
		System.arraycopy(byteFinalPacks2, 0, o, n.length, byteFinalPacks2.length);
		
		byte[] byteFinalPacks3 = PackedConvertor.pack("000");
		
		byte[] p = new byte[o.length + byteFinalPacks3.length];
		System.arraycopy(o, 0, p, 0, o.length);
		System.arraycopy(byteFinalPacks3, 0, p, o.length, byteFinalPacks3.length);
		
		System.out.println("Intermediate Res array p -------->"+new String(p));
	FileOutputStream out = new FileOutputStream("D://Shab//RCCTXN.txt");  //write to file
		out.write(p);

	}

}
