import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class TimeCalc {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:SS");
		Date Curdate = new Date();
		String res = formatter.format(Curdate);
		System.out.println(res);
		
		TimeZone tz = TimeZone.getDefault();
		
		if (tz.inDaylightTime(Curdate)) {
			System.out.println(res+"-05:00");
		} else {
			System.out.println(res+"-06:00");
		}

	}

}
