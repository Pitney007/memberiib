BROKER SCHEMA com.eh.esb.iib.esql.utilities.logging
/* ------------------------------------------------------------------------------------
 *  COMPANY           :     EMBLEM HEALTH
 *  PROJECT           :     EH_ESB_IIB_LIB_CommonComponents
 *  ESQL NAME         :     Logging_CommonProcedures.esql
 * DESCRIPTION       :     This ESQL File contains a set of re-usable and commonly
  invokable procedures and functions used as a part of the
  MQAttach logging implementation.
 *-------------------------------------------------------------------------------------    
 * ******************        MODIFICATION HISTORY   ***********************************
 * ------------------------------------------------------------------------------------
 * VERSION  CRNUM   DATE(MM/DD/CCYY)   Author                      Comments
 * 1.0              03/05/2015         Prolifics Initial Creation
 * -----------------------------------------------------------------------------------*/

  /* ------------------------------------------------------------------------------
       * Func/Proc Name  : CreateAuditLogEvent
       * @Param Name     : None
       * @Return         : None
       * Description     : This procedure is used to create the audit log event.
                                   
       * Modification History:
       * DATE (MM/DD/CCYY)     Author                       Comments
       * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/

CREATE PROCEDURE CreateAuditLogEvent (INOUT rEnv REFERENCE)
BEGIN
/*Create intermediate MQA Log message*/
SET rEnv.MQA = NULL;
SET rEnv.MQA.CodedCharSetId = 437;
SET rEnv.MQA.Encoding = 546;
CREATE FIELD rEnv.MQA.LogEvent;
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
SET rMqaLogEvt.TransactionId = UUIDASCHAR; --'08ca6708-5764-40de-ad22-3b9e5297998c';
SET rMqaLogEvt.StatusCode = 0;
SET rMqaLogEvt.ResponseMsg = 'Successfull Transaction';
SET rMqaLogEvt.FlowStartTime = CURRENT_GMTTIMESTAMP;
CALL SetByteCount(0,rEnv);
-- Create Meta data for Job Name
CALL CreateMetaData('com.ibm.wmqfte.JobName',SQL.MessageFlowLabel,'MessageFlowName',rEnv);
END;

  /* ------------------------------------------------------------------------------
       * Func/Proc Name  : UpdateAuditLogEventWithError
       * @Param Name     : None
       * @Return         : None
       * Description     : This procedure is used to update the audit log with error
        details.
                                   
       * Modification History:
       * DATE (MM/DD/CCYY)     Author                       Comments
       * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/

CREATE PROCEDURE UpdateAuditLogEventWithError (IN cExceptionList CHARACTER, INOUT rEnv REFERENCE)
BEGIN
DECLARE rEnvErrorDetails REFERENCE TO rEnv.ErrorDetails;
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
/*Update intermediate MQA Log message*/
SET rMqaLogEvt.StatusCode = 1;
SET rMqaLogEvt.ResponseMsg = rEnvErrorDetails.ExceptionType;
-- Error Message Info
CREATE LASTCHILD OF rMqaLogEvt.ErrorDetails NAME 'ErrorMessage' VALUE  'ExceptionType' ||': ' || COALESCE(rEnvErrorDetails.ExceptionType,'');
CREATE LASTCHILD OF rMqaLogEvt.ErrorDetails NAME 'ErrorMessage' VALUE  'ErrorCode' ||': ' || CAST(rEnvErrorDetails.ErrorCode AS CHARACTER);
CREATE LASTCHILD OF rMqaLogEvt.ErrorDetails NAME 'ErrorMessage' VALUE  'ErrorText' ||': ' || COALESCE(rEnvErrorDetails.ErrorText,'');
CREATE LASTCHILD OF rMqaLogEvt.ErrorDetails NAME 'ExceptionListDetails' VALUE COALESCE(cExceptionList,'');
END;

/* ------------------------------------------------------------------------------
       * Func/Proc Name  : AddLogEvent
       * @Param Name     : None
       * @Return         : None
       * Description     : This procedure is used to create log events.
                                   
       * Modification History:
       * DATE (MM/DD/CCYY)     Author                       Comments
       * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/
CREATE PROCEDURE AddLogEvent (IN cNodeName CHARACTER, IN cLogDescr CHARACTER, INOUT rEnv REFERENCE)
BEGIN
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
DECLARE rEvntMsg REFERENCE TO rEnv;
CREATE LASTCHILD OF rMqaLogEvt.EventMessages AS rEvntMsg NAME 'EventMessage';
SET rEvntMsg.EventTimeStamp = CAST(CURRENT_GMTTIMESTAMP AS CHARACTER);
SET rEvntMsg.EventTimeStamp   = SUBSTRING (rEvntMsg.EventTimeStamp FROM 15 FOR 10)||'T'|| SUBSTRING (rEvntMsg.EventTimeStamp FROM 26 FOR 12 )||'Z';
SET rEvntMsg.NodeName = cNodeName;
SET rEvntMsg.LogDescription = cLogDescr;
END;

/* ------------------------------------------------------------------------------
       * Func/Proc Name  : AddInfoLogEvent
       * @Param Name     : None
       * @Return         : None
       * Description     : This procedure is used to create log events.
                                   
       * Modification History:
       * DATE (MM/DD/CCYY)     Author                       Comments
       *            Cognizant    Initial Creation
* -------------------------------------------------------------------------------*/
CREATE PROCEDURE AddInfoLogEvent (IN cNodeName CHARACTER, IN cLogDescr CHARACTER, INOUT rEnv REFERENCE)
BEGIN
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
DECLARE rEvntMsg REFERENCE TO rEnv.InfoLog;
CREATE LASTCHILD OF rMqaLogEvt.InfoEventMessages AS rEvntMsg NAME 'EventMessage';
SET rEvntMsg.EventTimeStamp = CAST(CURRENT_GMTTIMESTAMP AS CHARACTER);
SET rEvntMsg.EventTimeStamp   = SUBSTRING (rEvntMsg.EventTimeStamp FROM 15 FOR 10)||'T'|| SUBSTRING (rEvntMsg.EventTimeStamp FROM 26 FOR 12 )||'Z';
SET rEvntMsg.NodeName = cNodeName;
SET rEvntMsg.LogDescription = cLogDescr;
END;
/* ------------------------------------------------------------------------------
       * Func/Proc Name  : AddPayloadLogEvent
       * @Param Name     : None
       * @Return         : None
       * Description     : This procedure is used to create payload log events.
                                   
       * Modification History:
       * DATE (MM/DD/CCYY)     Author                       Comments
       * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/
CREATE PROCEDURE AddPayloadLogEvent (IN rMsgPayload REFERENCE,IN cNodeName CHARACTER,
IN cLogDescr CHARACTER, IN cLogFormat CHARACTER,
INOUT rEnv REFERENCE)
BEGIN
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
DECLARE rEvntMsg REFERENCE TO rMqaLogEvt;
-- Add log event
CALL AddLogEvent(cNodeName,cLogDescr,rEnv);
MOVE rEvntMsg TO rMqaLogEvt.EventMessages;
MOVE rEvntMsg LASTCHILD;
SET rEvntMsg.LogFormat = cLogFormat;
SET rEvntMsg.LogMessage = rMsgPayload;
END;
/* ------------------------------------------------------------------------------
     * Func/Proc Name   : CreateMetaData
* @Param Name     :
* @Return   :
* Description     : This procedure is used to create Meta data..
 
* Modification History :
* DATE (MM/DD/CCYY)     Author         Comments
     * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/  
CREATE PROCEDURE CreateMetaData(IN cKey CHARACTER, IN cValue CHARACTER, IN cKeyDecr CHARACTER, IN rEnv REFERENCE) BEGIN
DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
DECLARE rOutMetaDta REFERENCE TO rEnv;
-- MetaData Info
CREATE LASTCHILD OF rMqaLogEvt.MetaDataSet AS rOutMetaDta NAME 'MetaData';
SET rOutMetaDta.Key = cKey;
SET rOutMetaDta.Value = COALESCE(cValue,'NA');
SET rOutMetaDta.Name = cKeyDecr;
END;

/* ------------------------------------------------------------------------------
     * Func/Proc Name   : SetByteCount
* @Param Name     :
* @Return   :
* Description     : This procedure is used add lengh of the message which IIB
is sending to target system under audit log message.
 
* Modification History :
* DATE (MM/DD/CCYY)     Author         Comments
     * 03/02/2015            Prolifics    Initial Creation
* -------------------------------------------------------------------------------*/  
CREATE PROCEDURE SetByteCount(IN iByteCount INTEGER, IN rEnv REFERENCE) BEGIN

DECLARE rMqaLogEvt REFERENCE TO rEnv.MQA.LogEvent;
SET rMqaLogEvt.MessageByteCount = iByteCount;
END;
CREATE PROCEDURE ConstructPayLoadLog (IN inRef REFERENCE,IN NodeLabel CHARACTER, IN Status CHARACTER,IN Format CHARACTER, INOUT rEnv REFERENCE)
/* --------------------------------------------------------------------------------
* Func/Proc Name : ConstructPayLoadLog
* Description : This used to construct payloag log for MQAttach
* Input : Payload,NodeLabel, Status, Format and Environment Variables
* Output : Environment Variables
* Modification History :
* DATE (MM/DD/CCYY) Author Comments
Cognizant Initial Creation
--------------------------------------------------------------------------------*/
BEGIN
--Declare variable
DECLARE bLogMessage BLOB;
--convert the payload to BLOB format
SET bLogMessage = ASBITSTREAM(inRef CCSID rEnv.MQA.CodedCharSetId ENCODING rEnv.MQA.Encoding) ;
-- Log the message with payload
CALL AddPayloadLogEvent(bLogMessage,NodeLabel,Status,'xml',rEnv);
END;