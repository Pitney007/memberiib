package com.eh.esb.iib.esql.utilities;

import com.ibm.broker.plugin.MbTimestamp;

import java.util.Calendar;

public class LatestTime {

public static MbTimestamp getCurrentTime(){

Calendar now = Calendar.getInstance();

int year = now.get(Calendar.YEAR);

int month = now.get(Calendar.MONTH) + 1; // Note: zero based!

int day = now.get(Calendar.DAY_OF_MONTH);

int hour = now.get(Calendar.HOUR_OF_DAY);

int minute = now.get(Calendar.MINUTE);

int second = now.get(Calendar.SECOND);

int millis = now.get(Calendar.MILLISECOND);

return new MbTimestamp(year, month, day, hour, minute, second, millis);

}

}